libdbix-class-helpers-perl (2.037000-1) unstable; urgency=medium

  * Import upstream version 2.037000.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.
  * Update lintian override (format).

 -- gregor herrmann <gregoa@debian.org>  Sat, 16 Nov 2024 01:37:01 +0100

libdbix-class-helpers-perl (2.036000-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Update lintian overrides for renamed tags.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libaliased-perl,
      libdbix-class-candy-perl and libmoo-perl.
    + libdbix-class-helpers-perl: Drop versioned constraint on
      libdbix-class-candy-perl and libmoo-perl in Depends.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 16:03:22 +0000

libdbix-class-helpers-perl (2.036000-1) unstable; urgency=medium

  * Import upstream version 2.036000.

 -- gregor herrmann <gregoa@debian.org>  Sun, 29 Mar 2020 23:49:44 +0200

libdbix-class-helpers-perl (2.035000-1) unstable; urgency=medium

  * Import upstream version 2.035000.
  * Update debian/libdbix-class-helpers-perl.docs (renamed file).
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Set upstream metadata fields: Bug-Submit.

 -- gregor herrmann <gregoa@debian.org>  Tue, 25 Feb 2020 22:27:10 +0100

libdbix-class-helpers-perl (2.034002-1) unstable; urgency=medium

  * Import upstream version 2.034002.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Jan 2020 23:37:38 +0100

libdbix-class-helpers-perl (2.034001-1) unstable; urgency=medium

  * Import upstream version 2.034001.
  * Declare compliance with Debian Policy 4.4.1.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Tue, 05 Nov 2019 20:37:50 +0100

libdbix-class-helpers-perl (2.034000-1) unstable; urgency=medium

  * Import upstream version 2.034000.
  * Update years of upstream and packaging copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.0.
  * Drop unneeded version constraints from (build) dependencies.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Aug 2019 22:59:00 +0200

libdbix-class-helpers-perl (2.033004-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 2.033004.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Thu, 19 Apr 2018 20:53:24 +0200

libdbix-class-helpers-perl (2.033003-1) unstable; urgency=medium

  * Team upload.
  * debian/copyright: Update copyright years.
  * debian/control: Remove libstring-camelcase-perl from {Build-}Depends.
  * Bump Standards-Version to 4.0.0 (no changes)
  * Import upstream version 2.033003

 -- Angel Abad <angel@debian.org>  Mon, 03 Jul 2017 17:28:23 +0200

libdbix-class-helpers-perl (2.033002-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 2.033002
  * debian/copyright: Update years.

 -- Angel Abad <angel@debian.org>  Wed, 02 Nov 2016 16:15:39 +0100

libdbix-class-helpers-perl (2.033001-1) unstable; urgency=medium

  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Import upstream version 2.033001.
  * Drop patches, both merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Mon, 29 Aug 2016 20:57:34 +0200

libdbix-class-helpers-perl (2.033000-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Import upstream version 2.033000.
  * Update years of upstream and packaging copyright.
  * Update alternative and versioned (build) dependencies.
  * Add a patch and a lintian override for spelling mistakes.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Sun, 24 Jul 2016 17:38:12 +0200

libdbix-class-helpers-perl (2.032000-1) unstable; urgency=medium

  * Import upstream version 2.032000.
  * Add (build) dependency on libmoo-perl.
  * Drop version from libtest-roo-perl build dependency. Thanks to cme.

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Nov 2015 17:52:48 +0100

libdbix-class-helpers-perl (2.031000-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 2.031000
  * Remove no longer necessary abstract.patch
  * Bump debhelper compatibility level to 9

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Tue, 15 Sep 2015 22:11:04 -0300

libdbix-class-helpers-perl (2.030000-1) unstable; urgency=medium

  * Import upstream version 2.030000.

 -- gregor herrmann <gregoa@debian.org>  Sat, 04 Jul 2015 18:55:17 +0200

libdbix-class-helpers-perl (2.029000-1) unstable; urgency=medium

  * Import upstream version 2.029000.
  * Refresh 0001-Fix-a-strictness-error-in-a-deprecated-module.patch
    (offset).
  * Update years of upstream and packaging copyright.
  * Update (build) dependencies.
  * Add patch to add whatis entry to manpage.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Jun 2015 16:08:55 +0200

libdbix-class-helpers-perl (2.023007-1) unstable; urgency=medium

  * Imported upstream version 2.023007
  * Refresh 0001-Fix-a-strictness-error-in-a-deprecated-module.patch.
    Fuzz due to version number.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Wed, 08 Oct 2014 22:18:08 +0200

libdbix-class-helpers-perl (2.023006-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Drop debian/tests/control, add Testsuite field to debian/control
    instead.

  [ Niko Tyni ]
  * Slightly patch a deprecated module to fix a compile time error

 -- Niko Tyni <ntyni@debian.org>  Fri, 19 Sep 2014 12:58:49 +0300

libdbix-class-helpers-perl (2.023006-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Imported upstream version 2.023006
  * Add autopkgtest control file.

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Sep 2014 16:56:46 +0200

libdbix-class-helpers-perl (2.023005-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Tue, 12 Aug 2014 23:39:37 +0200

libdbix-class-helpers-perl (2.023004-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Aug 2014 14:05:23 +0200

libdbix-class-helpers-perl (2.023003-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 25 Jul 2014 20:45:40 +0200

libdbix-class-helpers-perl (2.023002-1) unstable; urgency=medium

  * New upstream release.
  * Make build dependency on libtest-roo-perl versioned.

 -- gregor herrmann <gregoa@debian.org>  Sun, 29 Jun 2014 16:01:37 +0200

libdbix-class-helpers-perl (2.023001-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 Jun 2014 14:30:33 +0200

libdbix-class-helpers-perl (2.023000-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Thu, 29 May 2014 19:41:38 +0200

libdbix-class-helpers-perl (2.022000-1) unstable; urgency=medium

  * New upstream release.
  * Drop pod-whatis.patch patch, fixed upstream.

 -- gregor herrmann <gregoa@debian.org>  Mon, 05 May 2014 22:09:09 +0200

libdbix-class-helpers-perl (2.021001-1) unstable; urgency=medium

  * New upstream release.
  * Install CONTRIBUTING file.
  * Strip trailing slash from metacpan URLs.
  * Update (build) dependencies.
  * Drop pod-whatis.patch, merged upstream.
  * Add patch to fix some new POD issues.

 -- gregor herrmann <gregoa@debian.org>  Mon, 07 Apr 2014 20:04:30 +0200

libdbix-class-helpers-perl (2.019004-1) unstable; urgency=medium

  * New upstream release.
  * Bump versioned (build) dependency on libdbix-class-perl.
  * Add build dependency on libtest-fatal-perl.
  * Add patch to add missing whatis entry to a manpage.

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Feb 2014 00:18:19 +0100

libdbix-class-helpers-perl (2.019002-1) unstable; urgency=medium

  * New upstream release.
  * Update years of copyright.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Wed, 15 Jan 2014 16:46:01 +0100

libdbix-class-helpers-perl (2.019001-1) unstable; urgency=medium

  * New upstream release.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Wed, 11 Dec 2013 19:33:36 +0100

libdbix-class-helpers-perl (2.019000-1) unstable; urgency=low

  * New upstream release.
  * Add build dependency on libdatetime-format-sqlite-perl.

 -- gregor herrmann <gregoa@debian.org>  Fri, 18 Oct 2013 18:29:13 +0200

libdbix-class-helpers-perl (2.018003-1) unstable; urgency=low

  * New upstream release.
  * Drop runtime dependency on libdbd-sqlite3-perl.

 -- gregor herrmann <gregoa@debian.org>  Mon, 30 Sep 2013 17:21:40 +0200

libdbix-class-helpers-perl (2.018002-1) unstable; urgency=low

  * New upstream release.
  * Drop whatis.patch, merged upstream.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 21 Sep 2013 23:41:33 +0200

libdbix-class-helpers-perl (2.017000-1) unstable; urgency=low

  [ intrigeri ]
  * Imported Upstream version 2.013003

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release 2.017000.
    Fixes "FTBFS with perl 5.18: t/schema/lint-contents.t"
    (Closes: #709789)
  * Update years of upstream and packaging copyright.
  * Set Standards-Version to 3.9.4 (no changes).
  * Update build dependencies.
  * Don't install empty manpages.
  * Add a patch to add missing whatis entries to manpages.

 -- gregor herrmann <gregoa@debian.org>  Sun, 26 May 2013 19:24:14 +0200

libdbix-class-helpers-perl (2.013002-1) unstable; urgency=low

  * New upstream release.
  * Add build dependency on libsql-translator-perl (needed by one test
    under some circumstances).

 -- gregor herrmann <gregoa@debian.org>  Mon, 25 Jun 2012 18:58:40 +0200

libdbix-class-helpers-perl (2.013000-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 08 Jun 2012 21:24:57 +0200

libdbix-class-helpers-perl (2.011000-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Mon, 04 Jun 2012 19:22:06 +0200

libdbix-class-helpers-perl (2.010001-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 26 May 2012 21:39:18 +0200

libdbix-class-helpers-perl (2.010000-1) unstable; urgency=low

  * Imported Upstream version 2.010000
  * Added myself to Uploaders and Copyright
  * debian/control:
     + added version (>= 6.04) to libcarp-clan-perl in B-D-I and Depends
     + added libnamespace-clean-perl (>= 0.23) to B-D-I and Depends
  * Removed spelling.patch: it's now included in source code

 -- Fabrizio Regalli <fabreg@fabreg.it>  Sat, 26 May 2012 18:16:32 +0200

libdbix-class-helpers-perl (2.007004-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 13 Apr 2012 16:31:18 +0200

libdbix-class-helpers-perl (2.007003-1) unstable; urgency=low

  * Initial release (closes: #590476).

 -- gregor herrmann <gregoa@debian.org>  Fri, 06 Apr 2012 01:15:36 +0200
